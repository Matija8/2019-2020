const express = require('express');
const users = require('./users.js');

const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.set('views', 'views/');

app.get('/users', (req, res) => {
  res.render('users.ejs', { title: 'All users', users: users });
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
