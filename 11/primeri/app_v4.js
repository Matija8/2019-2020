const express = require('express');
const users = require('./users.js');
const { urlencoded, json } = require('body-parser');

const app = express();
const port = 3000;

app.use(json());
app.use(urlencoded({ extended: false }));

// http://localhost:3000/api/users
app.get('/api/users', (req, res) => {
  res.json(users);
});

// http://localhost:3000/api/users/4
app.get('/api/users/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const user = users.filter((user) => user.id == parseInt(req.params.id));
    res.status(200);
    res.json(user);
  } else {
    res.status(404);
    res.send();
  }
});

// http://localhost:3000/api/users
app.post('/api/users', (req, res) => {
  // console.log(req.body);
  if (!req.body.username || !req.body.password || !req.body.email) {
    res.status(400);
    res.send();
  } else {
    const newUser = {
      id: 23,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      status: 'active'
    };

    users.push(newUser);

    res.status(201).json(newUser);
  }
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
