const express = require('express');
const users = require('./users.js');

const app = express();
const port = 3000;

// http://localhost:3000/api/users
app.get('/api/users', (req, res) => {
  // res.status(200);
  // res.json(users);

  res.status(200).json(users);
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
