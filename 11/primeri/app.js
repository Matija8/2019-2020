const express = require('express');
const {urlencoded, json} = require('body-parser');

const app = express();
const port = 3000;

app.use(json());
app.use(urlencoded({extended: false}));

const usersRoutes = require('./routes/api/users');
app.use('/api/users', usersRoutes);

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});