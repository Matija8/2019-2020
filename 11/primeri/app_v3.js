const express = require('express');
const users = require('./users.js');

const app = express();
const port = 3000;

// http://localhost:3000/api/users
app.get('/api/users', (req, res) => {
  res.json(users);
});

// http://localhost:3000/api/users/4
app.get('/api/users/:id', (req, res) => {
  // console.log(req.params);
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const user = users.filter((user) => user.id == parseInt(req.params.id));
    res.status(200);
    res.json(user);
  } else {
    res.status(404);
    res.send();
  }
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
