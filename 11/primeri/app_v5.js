const express = require('express');
const users = require('./users.js');
const { urlencoded, json } = require('body-parser');

const app = express();
const port = 3000;

app.use(json());
app.use(urlencoded({ extended: false }));

// http://localhost:3000/api/users
app.get('/api/users', (req, res) => {
  res.json(users);
});

// http://localhost:3000/api/users/4
app.get('/api/users/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const user = users.filter((user) => user.id == parseInt(req.params.id));
    res.status(200);
    res.json(user);
  } else {
    res.status(404);
    res.send();
  }
});

// http://localhost:3000/api/users
app.post('/api/users', (req, res) => {
  if (!req.body.username || !req.body.password) {
    res.status(400);
    res.send();
  } else {
    const newUser = {
      id: 23,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      status: 'active',
    };

    users.push(newUser);

    res.status(201).json(newUser);
  }
});

// http://localhost:3000/api/users/4
app.put('/api/users/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const updatePasswordInfo = req.body;

    users.forEach((user) => {
      if (user.id == parseInt(req.params.id)) {
        if (updatePasswordInfo.currentPassword != user.password) {
          res.status(400).send();
        } else {
          user.password = updatePasswordInfo.newPassword;
          res.status(200).send();
        }
      }
    });
  } else {
    res.status(404).send();
  }
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
