const express = require('express');
const users = require('./users.js');

const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.set('views', 'views/');

app.get('/users/:id', (req, res) => {
  const usersWithId = users.filter(
    (user) => user.id == parseInt(req.params.id)
  );
  const user = usersWithId.length == 0 ? undefined : usersWithId[0];

  res.render('user.ejs', { user: user });
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
