const express = require('express');
const users = require('./users.js');

const app = express();
const port = 3000;

const logger = (req, res, next) => {
  console.log('Zahtev je generisan!');
  next();
};

app.use(logger);

app.get('/api/users', (req, res) => {
  res.status(200).json(users);
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
