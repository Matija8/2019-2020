import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kreator',
  templateUrl: './kreator.component.html',
  styleUrls: ['./kreator.component.css']
})
export class KreatorComponent implements OnInit {

  public imePrezime: string = '';
  public odabranSmer: string = 'I';

  constructor() { }

  ngOnInit() {
  }

  onSelectedSmer(event: Event): void {
    this.odabranSmer = (<HTMLSelectElement>event.target).value;
  }

}
