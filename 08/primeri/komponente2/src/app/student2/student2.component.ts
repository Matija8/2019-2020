import { Component, OnInit, Input } from '@angular/core';
import { Student, PogresanSmerError } from '../../models/student.model';

@Component({
  selector: 'app-student2',
  templateUrl: './student2.component.html',
  styleUrls: ['./student2.component.css']
})
export class Student2Component implements OnInit {

  @Input('studentData')
  public student: Student;

  constructor() { }

  ngOnInit() {
  }

  dohvatiUrlSlike(): string {
    switch (this.student.inicijalSmera) {
      case 'I':
        return 'assets/blue.png';
      case 'M':
        return 'assets/red.png';
      case 'A':
        return 'assets/green.png';
      case 'T':
        return 'assets/gray.png';
      default:
        throw new PogresanSmerError(`Nepoznat inicijal smera ${this.student.inicijalSmera}`);
    }
  }

}
