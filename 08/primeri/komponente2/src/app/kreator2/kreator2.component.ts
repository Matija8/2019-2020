import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Student } from 'src/models/student.model';

@Component({
    selector: 'app-kreator2',
    templateUrl: './kreator2.component.html',
    styleUrls: ['./kreator2.component.css']
})
export class Kreator2Component implements OnInit {

    public imePrezime: string = '';
    public odabranSmer: string = 'I';

    @Output('noviStudent')
    public emitNoviStudent: EventEmitter<Student> = new EventEmitter<Student>();

    constructor() { }

    ngOnInit() {
    }

    onSelectedSmer(event: Event): void {
        this.odabranSmer = (<HTMLSelectElement>event.target).value;
    }

    onUpisStudenta(): void {
        if (this.imePrezime === '') {
            window.alert('Molimo unesite ime i prezime novog studenta!');
            return;
        }
        const noviStudent = new Student(this.imePrezime, this.odabranSmer);
        this.emitNoviStudent.emit(noviStudent);
        this.imePrezime = '';
    }

}
