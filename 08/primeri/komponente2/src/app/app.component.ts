import { Component } from '@angular/core';
import { Student } from '../models/student.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  studenti: Array<Student> = [];

  constructor() {
    this.studenti.push(new Student('TestImePrezime', 'T'));
  }

  onNoviStudent(student: Student): void {
    this.studenti.push(student);
  }

  onUkloniPrvogStudenta(): void {
    this.studenti.splice(0, 1);
  }
}
