import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { KreatorComponent } from './kreator/kreator.component';
import { StudentComponent } from './student/student.component';
import { Kreator2Component } from './kreator2/kreator2.component';
import { Student2Component } from './student2/student2.component';
import { Kreator3Component } from './kreator3/kreator3.component';
import { Kreator4Component } from './kreator4/kreator4.component';
import { Student3Component } from './student3/student3.component';

@NgModule({
  declarations: [
    AppComponent,
    KreatorComponent,
    StudentComponent,
    Kreator2Component,
    Student2Component,
    Kreator3Component,
    Kreator4Component,
    Student3Component
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
