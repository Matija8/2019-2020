import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Student } from '../../models/student.model';

@Component({
  selector: 'app-kreator3',
  templateUrl: './kreator3.component.html',
  styleUrls: ['./kreator3.component.css']
})
export class Kreator3Component implements OnInit {

  @Output('noviStudent')
  public emitNoviStudent: EventEmitter<Student> = new EventEmitter<Student>();

  constructor() { }

  ngOnInit() {
  }

  onUpisStudenta(imePrezimeInput: HTMLInputElement, odabranSmerSelect: HTMLSelectElement): void {
    const imePrezime: string = imePrezimeInput.value;
    const odabranSmer: string = odabranSmerSelect.value;

    if (imePrezime === '') {
      window.alert('Molimo unesite ime i prezime novog studenta!');
      return;
    }

    const noviStudent = new Student(imePrezime, odabranSmer);
    this.emitNoviStudent.emit(noviStudent);

    // Ne preporucuje se menjanje DOM stabla na ovaj nacin
    imePrezimeInput.value = '';
  }

}
