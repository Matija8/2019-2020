export class PogresanSmerError extends Error {};

export class Student {
    private imePrezime: string;
    private smer: string;
    private datumUpisa: string;

    constructor(imePrezime: string, smer: string) {
        this.imePrezime = imePrezime;
        this.smer = Student.imeSmera(smer);
        this.datumUpisa = Student.kreirajDatum();
    }

    public getImePrezime(): string {
        return this.imePrezime;
    }

    public getSmer(): string {
        return this.smer;
    }

    public getDatumUpisa(): string {
        return this.datumUpisa;
    }

    public get inicijalSmera(): string {
        return this.getSmer()[0];
    }

    private static kreirajDatum(): string {
        const sada: Date = new Date(Date.now());
        return `${sada.getDate()}.${sada.getMonth()+1}.${sada.getFullYear()}. u ${sada.getHours()}:${sada.getMinutes()}h`;
    }

    private static imeSmera(skracenica: string): string {
        switch (skracenica) {
            case 'I':
                return 'Informatika';
            case 'M':
                return 'Matematika';
            case 'A':
                return 'Astronomija';
            case 'T':
                return 'TestSmer';
            default:
                throw new PogresanSmerError(`Ne postoji skracenica ${skracenica} za smerove na Matematickom fakultetu!`);
        }
    }
}