import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgifelseTestComponent } from './ngifelse-test.component';

describe('NgifelseTestComponent', () => {
  let component: NgifelseTestComponent;
  let fixture: ComponentFixture<NgifelseTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgifelseTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgifelseTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
