import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-ngstyle-test',
    templateUrl: './ngstyle-test.component.html',
    styleUrls: ['./ngstyle-test.component.css']
})
export class NgstyleTestComponent implements OnInit {

    private static readonly listaImena: Array<string> = ['Una', 'Milica', 'Marko', 'Bojan'];
    private static readonly brojImena: number = NgstyleTestComponent.listaImena.length;

    private ocene: Array<number> = [];
    private ime: string = '';
    private ocenaStr: string = null;

    constructor() {
        this.ime = NgstyleTestComponent.listaImena[Math.floor(Math.random() * NgstyleTestComponent.brojImena)];
    }

    ngOnInit() {
    }

    private prosek(): number {
        if (this.ocene.length === 0) {
            return 0.0;
        }
        return this.ocene.reduce((prev, next) => prev + next) / this.ocene.length;
    }

    private onDodajOcenu(): void {
        if (Number.isNaN(Number.parseInt(this.ocenaStr))) {
            this.ocenaStr = null;
            return;
        }

        const ocenaNum: number = Number.parseInt(this.ocenaStr);
        if (ocenaNum < 5 || ocenaNum > 10) {
            this.ocenaStr = null;
            return;
        }

        this.ocene.push(ocenaNum);
        this.ocenaStr = null;
    }

    private izracunajStil() {
        let colorChoice: string,
            fontSizeChoice: string;
        
        const prosek = this.prosek();
        if (prosek >= 9) {
            colorChoice = 'green';
            fontSizeChoice = '20px';
        } else if (prosek >= 8) {
            colorChoice = 'blue';
            fontSizeChoice = '19px';
        } else if (prosek >= 7) {
            colorChoice = 'cyan';
            fontSizeChoice = '18px';
        } else if (prosek >= 6) {
            colorChoice = 'yellow';
            fontSizeChoice = '17px';
        } else if (prosek >= 5) {
            colorChoice = 'red';
            fontSizeChoice = '16px';
        } else {
            colorChoice = 'black';
            fontSizeChoice = '15px';
        }
        
        return {
            color:  colorChoice,
            'font-size': fontSizeChoice
        }
    }

}
