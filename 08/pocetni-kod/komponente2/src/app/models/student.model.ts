import { PogresanSmerError } from './pogresan-smer-error.model';

export class Student {
  constructor(private imePrezime: string, private smer: string) {
    this.datumUpisa = Student.kreirajDatum();
  }

  public get inicijalSmera(): string {
    return this.getSmer()[0];
  }
  private datumUpisa: string;

  private static kreirajDatum(): string {
    const sada = new Date(Date.now());
    return `${sada.getDate()}.${sada.getMonth() +
      1}.${sada.getFullYear()}. u ${sada.getHours()}:${sada.getMinutes()}h`;
  }

  private static imeSmera(skraćenica: string): string {
    switch (skraćenica) {
      case 'I':
        return 'Informatika';
      case 'M':
        return 'Matematika';
      case 'A':
        return 'Astronomija';
      case 'T':
        return 'TestSmer';
      default:
        throw new PogresanSmerError(
          `Ne postoji skraćena ${skraćenica} za smerove na Matematičkom fakultetu`
        );
    }
  }

  public getImePrezime(): string {
    return this.imePrezime;
  }

  public getSmer(): string {
    return this.smer;
  }

  public getDatumUpisa(): string {
    return this.datumUpisa;
  }
}
