import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kreator',
  templateUrl: './kreator.component.html',
  styleUrls: ['./kreator.component.css']
})
export class KreatorComponent implements OnInit {
  public imePrezime: string;
  public odabranSmer: string;

  constructor() {
    this.imePrezime = '';
    this.odabranSmer = 'I';
  }

  ngOnInit() {}

  onSelectedSmer(event: Event): void {
    this.odabranSmer = (event.target as HTMLSelectElement).value;
  }
}
