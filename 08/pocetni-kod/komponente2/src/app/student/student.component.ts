import { Component, OnInit } from '@angular/core';
import { Student } from './../models/student.model';
import { PogresanSmerError } from '../models/pogresan-smer-error.model';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  public student: Student = new Student('TestImePrezime', 'T');

  constructor() {}

  ngOnInit() {}

  dohvatiUrlSlike(): string {
    switch (this.student.inicijalSmera) {
      case 'I':
        return 'assets/blue.png';
      case 'M':
        return 'assets/red.png';
      case 'A':
        return 'assets/green.png';
      case 'T':
        return 'assets/gray.png';
      default:
        throw new PogresanSmerError(
          `Nepoznat inicijal smera ${this.student.inicijalSmera}`
        );
    }
  }
}
