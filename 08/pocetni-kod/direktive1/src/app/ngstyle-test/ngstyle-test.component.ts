import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngstyle-test',
  templateUrl: './ngstyle-test.component.html',
  styleUrls: ['./ngstyle-test.component.css']
})
export class NgstyleTestComponent implements OnInit {
  private static readonly listaImena: string[] = [
    'Una',
    'Milica',
    'Marko',
    'Bojan'
  ];
  private static readonly brojImena: number =
    NgstyleTestComponent.listaImena.length;

  private ocene: number[] = [];
  public ime: string;
  public ocenaStr: string;

  constructor() {
    this.ime =
      NgstyleTestComponent.listaImena[
        Math.floor(Math.random() * NgstyleTestComponent.brojImena)
      ];
  }

  ngOnInit() {}

  public prosek(): number {
    return this.ocene.length === 0
      ? 0
      : this.ocene.reduce((prev, next) => prev + next, 0) / this.ocene.length;
  }

  public onDodajOcenu(): void {
    const ocenaNum = Number.parseInt(this.ocenaStr, 10);
    this.ocenaStr = undefined;
    if (Number.isNaN(ocenaNum) || ocenaNum < 5 || ocenaNum > 10) {
      return;
    }

    this.ocene.push(ocenaNum);
  }

  

}
