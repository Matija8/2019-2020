import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngfor-test',
  templateUrl: './ngfor-test.component.html',
  styleUrls: ['./ngfor-test.component.css']
})
export class NgforTestComponent implements OnInit {
  private static readonly listaImena: string[] = [
    'Una',
    'Milica',
    'Marko',
    'Bojan'
  ];
  private static readonly brojImena: number =
    NgforTestComponent.listaImena.length;

  private ocene: number[] = [];
  public ime: string;
  public ocenaStr: string;

  constructor() {
    this.ime =
      NgforTestComponent.listaImena[
        Math.floor(Math.random() * NgforTestComponent.brojImena)
      ];
  }

  ngOnInit() {}

  public prosek(): number {
    return this.ocene.length === 0
      ? 0
      : this.ocene.reduce((prev, next) => prev + next, 0) / this.ocene.length;
  }

  public onDodajOcenu(): void {
    const ocenaNum = Number.parseInt(this.ocenaStr, 10);
    this.ocenaStr = undefined;
    if (Number.isNaN(ocenaNum) || ocenaNum < 5 || ocenaNum > 10) {
      return;
    }

    this.ocene.push(ocenaNum);
  }
}
