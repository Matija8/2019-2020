import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngswitch-test',
  templateUrl: './ngswitch-test.component.html',
  styleUrls: ['./ngswitch-test.component.css']
})
export class NgswitchTestComponent implements OnInit {
  private static readonly listaImena: string[] = [
    'Una',
    'Milica',
    'Marko',
    'Bojan'
  ];
  private static readonly brojImena: number =
    NgswitchTestComponent.listaImena.length;

  private ocene: number[] = [];
  public ime: string;
  public ocenaStr: string;

  constructor() {
    this.ime =
      NgswitchTestComponent.listaImena[
        Math.floor(Math.random() * NgswitchTestComponent.brojImena)
      ];
  }

  ngOnInit() {}

  public prosek(): number {
    return this.ocene.length === 0
      ? 0
      : this.ocene.reduce((prev, next) => prev + next, 0) / this.ocene.length;
  }

  public onDodajOcenu(): void {
    const ocenaNum = Number.parseInt(this.ocenaStr, 10);
    this.ocenaStr = undefined;
    if (Number.isNaN(ocenaNum) || ocenaNum < 5 || ocenaNum > 10) {
      return;
    }

    this.ocene.push(ocenaNum);
  }

  

}
