## Angular - Pitanja za deo 2

1. Šta su Angular direktive i koje vrste postoje? Ukratko ih objasniti.
2. Zašto se komponente ubrajaju u direktive? Kako se one drugačije nazivaju i koji je smisao toga?
3. U čemu je razlika između komponenti i strukturnih direktiva?
4. Koja su dva načina da se referiše na direktive?
5. Koliko strukturnih direktiva može da se primeni nad jednim elementom?
6. Šta je direktiva `NgIf` i čemu odgovara njeno ponašanje u višim programskim jezicima?
7. Šta nam direktiva `NgIf` omogućava u slučaju da je vrednost našeg Bulovog izraza netačna?
8. Šta je direktiva `NgForOf` i čemu odgovara njeno ponašanje u višim programskim jezicima?
9. Koja sintaksa odgovara upotrebi `NgForOf`?
10. Šta su rangovske promenljive koje nam nudi `NgForOf`? Navesti primere.
11. Šta je direktiva `NgSwitch` i čemu odgovara njeno ponašanje u višim programskim jezicima?
12. Zašto se `*` koristi uz `NgSwitchCase` i `NgSwitchDefault`, ali ne uz `NgSwitch`?
13. Koja je uloga zvezdastog prefiksa?
14. Šta predstavljaju strukturne direktive, u svojoj punoj sintaksičkoj varijanti?
15. Napisati sledeći deo koda u punoj sintaksičkoj varijanti i opisati šta se dogodilo:
```html
<div *ngIf=”isVisible”> I am visible!</div>
```
16. Kada se primenjuju atributske direktive? Koliko atributskih direktiva može da se primeni nad jednim elementom?
17. Šta je direktiva `NgClass`?
18. Šta je direktiva `NgStyle` i kako izgleda njena vrednost?
19. Objasniti prirodni tok podataka između dve komponente. Kada se on ostvaruje?
20. Šta predstavlja ulazni tok? Koja je procedura za kreiranje ulaznog toka?
21. Šta predstavlja izlazni tok? Koja je procedura za kreiranje izlaznog toka?
22. Šta su referencne promenljive šablona i kada se koriste?
23. Kada koristimo dekorator `@ViewChild`? Koji su njegovi argumenti?
24. Šta je `ElementRef`?
25. Navesti primere operacija koje Angular izvršava u toku životnog veka neke komponente.
26. Objasniti osluškivanje životnog toka.
27. Koji su to trenuci koji su važni u životnom toku komponente?