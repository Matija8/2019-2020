## Angular - Pitanja za deo 1

1. Šta je Angular i kakve sve mogućnosti nudi?
2. Šta znači da su sve Angular aplikacije jednostranične aplikacije?
3. Šta je bootstraping?
4. Koji ključni elementi modula se definišu dekoratorom `@NgModule`? Objasniti ih ukratko.
5. Kako Angular zna da treba da prikaže HTML sadržaj komponente `AppComponent` na stranici?
6. Kako se u Angularu predstavljaju komponente? Šta je potrebno uraditi da bi Angular znao da nešto predstavlja komponentu?
7. Definisati komponentu `zanimanje` čija je HTML struktura lista najpoželjnijih zanimanja u 2019. godini, a na koju se može referisati selektorom `app-zanimanje`. Stil se nalazi u datoteci `zanimanje.component.css`.
8. Kako se mehanizmi vezivanja podataka dele prema usmerenosti?
9. Kako se mehanizmi vezivanja podataka dele prema kardinalnosti?
10. Šta je interpolacija niski? Objasniti njegovu sintaksu. 
11. Šta je vezivanje atributa? Objasniti njegovu sintaksu. 
12. Dati kod prepraviti tako da se umesto interpolacije niski koristi vezivanje atributa, a da ponašanje ostane neizmenjeno:
    
```typescript
import { Component } from '@angular/core';

@Component({
  selector:'my-app',
  template:`
    <h1>{{fullName}}</h1>
  `
})
export class AppComponent{
  fullName: string = 'Robert Junior';
}
```

13. Koja je razlika između interpolacije niski i vezivanja atributa?
14. Šta je vezivanje događaja? Objasniti njegovu sintaksu. Šta je parametar `$event`?
15. Dati kod prepraviti tako da se umesto interpolacije niski koristi vezivanje atributa, a da ponašanje ostane neizmenjeno:

```typescript
import { Component } from '@angular/core';

@Component({
  selector:'app-root',
  template:`
    <div>
      <img src="{{ imagePath }}" alt="cat gif">
    </div>
  `
})
export class AppComponent{
  imagePath: string = 'https://media.giphy.com/media/VbnUQpnihPSIgIXuZv/giphy.gif';
}
```

16. Šta je vezivanje događaja? Kada se koristi? Koja mu sintaksa odgovara?
17. Šta je dvosmerno vezivanje? Objasniti njegovu sintaksu. 
18. Koji su koraci potrebni da bi mogla da se iskoristi `ngModel` direktiva?
19. Šta su šablonski izrazi i koje smernice treba da isprate?
20. Zašto je u Angularu prioritetizovan TypeScript, a ne JavaScript?
