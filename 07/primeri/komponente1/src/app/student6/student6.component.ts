import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-student6',
    templateUrl: './student6.component.html',
    styleUrls: ['./student6.component.css']
})
export class Student6Component implements OnInit {

    private ime: string;
    private menjanjeJeDozvoljeno: boolean = true;

    constructor() { }

    ngOnInit() {
    }

    unosJeOnemogucen(): boolean {
        return !this.menjanjeJeDozvoljeno;
    }

    onSacuvajInformacije(): void {
        this.menjanjeJeDozvoljeno = false;
    }

    onIzmeniIme(event: Event): void {
        this.ime = (<HTMLInputElement>event.target).value;
    }

}
