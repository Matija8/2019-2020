import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Student1ListaComponent } from './student1-lista.component';

describe('Student1ListaComponent', () => {
  let component: Student1ListaComponent;
  let fixture: ComponentFixture<Student1ListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Student1ListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Student1ListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
