import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student7',
  templateUrl: './student7.component.html',
  styleUrls: ['./student7.component.css']
})
export class Student7Component implements OnInit {

    private ime: string;
    private menjanjeJeDozvoljeno: boolean = true;

    constructor() { }

    ngOnInit() {
    }

    unosJeOnemogucen(): boolean {
        return !this.menjanjeJeDozvoljeno;
    }

    onSacuvajInformacije(): void {
        this.menjanjeJeDozvoljeno = false;
    }

    onIzmeniIme(event: Event): void {
        this.ime = (<HTMLInputElement>event.target).value;
    }

}
