import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { Order } from './order.model';
import { Product } from '../product/product.model';
import { HttpErrorHandler } from '../utils/http-error-handler.model';

@Injectable({
  providedIn: 'root',
})
export class CartService extends HttpErrorHandler {
  private items: Product[] = [];
  private readonly ordersUrl = 'http://localhost:3000/orders/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
  }

  public addToCart(product: Product): void {
    this.items.push(product);
  }

  public getItems(): Product[] {
    return this.items;
  }

  public clearCart(): Product[] {
    this.items = [];
    return this.items;
  }

  public removeProductFromCartById(id: string): Product[] {
    this.items = this.items.filter((p: Product) => p._id === id);
    return this.items;
  }

  public createAnOrder(formData): Observable<Order> {
    const body = { ...formData, products: this.items };
    return this.http
      .post<Order>(this.ordersUrl, body)
      .pipe(catchError(super.handleError()));
  }

  getOrders(): Observable<Order[]> {
    return this.http
      .get<Order[]>(this.ordersUrl)
      .pipe(catchError(super.handleError()));
  }

  getOrderById(id: string): Observable<Order> {
    return this.http
      .get<Order>(this.ordersUrl + id)
      .pipe(catchError(super.handleError()));
  }

  removeOrderById(id: string): Observable<Order[]> {
    return this.http.delete(this.ordersUrl + id).pipe(
      catchError(super.handleError()),
      switchMap(() => this.getOrders())
    );
  }
}
