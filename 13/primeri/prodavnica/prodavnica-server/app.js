const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const indexAPIRoutes = require('./components/index/indexAPI');
const productRoutes = require('./components/product/productsAPI');
const orderRoutes = require('./components/orders/ordersAPI');

// Kreiranje Express.js aplikacije
const app = express();

// Konekcija na MongoDB SUBP
mongoose.connect('mongodb://127.0.0.1:27017/PRODAVNICABP', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Parsiranje tela zahteva za dva formata:
// 1) application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
// 2) application/json
app.use(bodyParser.json({}));

// Implementacija CORS zastite
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  if (req.method === 'OPTIONS') {
    res.header(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PATCH, DELETE'
    );

    return res.status(200).json({});
  }

  next();
});

// Definisanje osnovnih pravila za rutiranje
app.use('/', indexAPIRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// Obrada zahteva koji se ne poklapa sa nekom pravilom od iznad
app.use(function (req, res, next) {
  const error = new Error('Zahtev nije podrzan od servera');
  error.status = 405;

  next(error);
});

// Obrada svih gresaka u nasoj aplikaciji
app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
  res.status(statusCode).json({
    error: {
      message: error.message,
      status: statusCode,
      stack: error.stack
    },
  });
});

// Izvozenje Express.js aplikacije radi pokretanja servera
module.exports = app;
