const express = require('express');

const router = express.Router();

const controller = require('./productsController');

router.get('/', controller.getProducts);
router.post('/', controller.createAProduct);

router.get('/:productId', controller.getByProductId);
router.patch('/:productId', controller.updateByProductId);
router.delete('/:productId', controller.deleteByProductId);

module.exports = router;
