const mongoose = require('mongoose');

const Product = require('./productModel');
const Order = require('../orders/orderModel');

module.exports.getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).sort({ name: 1, price: -1 }).exec();
    res.status(200).json(products);
  } catch (err) {
    next(err);
  }
};

module.exports.createAProduct = async function (req, res, next) {
  // Pravimo novu instancu Product modela
  // na osnovu prosledjenih parametara
  const productObject = {
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    price: req.body.price,
    description: req.body.description,
  };
  const product = new Product(productObject);

  try {
    const savedProduct = await product.save();
    res.status(201).json({
      message: 'A product is successfully created',
      product: savedProduct,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.getByProductId = async function (req, res, next) {
  const productId = req.params.productId;

  try {
    const product = await Product.findById(productId).exec();

    // Ukoliko nije pronadjen objekat, prijavi 404
    if (!product) {
      return res
        .status(404)
        .json({ message: 'The product with given id does not exist' });
    }

    // Inace, vrati pronadjeni objekat
    res.status(200).json(product);
  } catch (err) {
    next(err);
  }
};

module.exports.updateByProductId = async function (req, res, next) {
  const productId = req.params.productId;

  // Procitaj iz tela zahteva sva polja u BP koja je potrebno izmeniti
  // i kreiraj upit za azuriranje na osnovu tih podataka
  const updateOptions = {};
  for (let i = 0; i < req.body.length; ++i) {
    let option = req.body[i];
    updateOptions[option.nazivPolja] = option.novaVrednost;
  }

  try {
    await Product.updateOne({ _id: productId }, { $set: updateOptions }).exec();
    res.status(200).json({ message: 'The product is successfully updated' });
  } catch (err) {
    next(err);
  }
};

module.exports.deleteByProductId = async function (req, res, next) {
  const productId = req.params.productId;

  try {
    // Prvo brisemo element ciji je identifikator prosledjeni ID proizvoda
    await Product.deleteOne({ _id: productId }).exec();
    // Zatim brisemo sve porudzbine
    // u cijem se nizu "products" nalazi prosledjeni ID proizvoda.
    // Vise o pisanju upita sa nizovima: https://docs.mongodb.com/manual/tutorial/query-arrays/
    await Order.deleteMany({ products: productId }).exec();

    res.status(200).json({ message: 'The product is successfully deleted' });
  } catch (err) {
    next(err);
  }
};

// Transakcije nece biti na ispitu! Ovo je samo za zainteresovane :)
// Da bi se iskoristila implementacija metoda iznad
// uz rad sa transakcijama, potrebno je koristiti replikaciju BP.
// Vise o replikacijama u MongoDB: https://docs.mongodb.com/manual/replication/
// Uputstvo za podesavanje: http://thecodebarbarian.com/introducing-run-rs-zero-config-mongodb-runner
// Transakcije u Mongoose biblioteci: https://mongoosejs.com/docs/transactions.html
module.exports.deleteByProductIdWithTransactions = async function (
  req,
  res,
  next
) {
  const productId = req.params.productId;
  // Zapocni novu sesiju
  const session = await mongoose.startSession();
  // Zapocni transakciju
  session.startTransaction();

  try {
    // Svakoj operaciji koja ucestvuje u transakciji
    // potrebno je da se prosledi opcija 'session' koja sadrzi objekat sesije.
    // Spisak svih opcija za razne operacije:
    // https://mongoosejs.com/docs/api.html#query_Query-setOptions
    await Product.deleteOne({ _id: productId }, { session }).exec();
    await Order.deleteMany({ products: productId }, { session }).exec();

    // Ako su se obe operacije uspesno zavrsile, potvrdjujemo izmene - COMMIT operacija
    await session.commitTransaction();
    session.endSession();

    res.status(200).json({ message: 'The product is successfully deleted' });
  } catch (err) {
    // Ako se bilo koja operacija izvrsila neuspesno, ponistavamo izmene - ROLLBACK operacija
    await session.abortTransaction();
    session.endSession();

    next(err);
  }
};
