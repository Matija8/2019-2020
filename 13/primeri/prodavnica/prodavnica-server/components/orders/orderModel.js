// U ovom fajlu definisemo model koji ce imati nase porudzbine
const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  products: [{
    type: mongoose.Schema.Types.ObjectId,
    // ref je naziv modela sa kojim se povezuje ovaj model
    ref: 'Product',
    required: true,
  }],
  name: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Order', orderSchema);
