# 9. sedmica vežbi

## Angular

- Sekcije 5.8, 5.9, 5.10 i 5.11 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}{:target="_blank"}
- [Kompletni primeri iz skripte](./primeri/){:target="_blank"}
- [Video lekcija - Nikola Ajzenhamer](https://www.youtube.com/watch?v=N2e-7FgbCVY){:target="_blank"}

### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"}

## Korisne veze

- [Zvanična stranica Angular razvojnog alata](https://angular.io/){:target="_blank"}
- [Biblioteka Bootstrap](https://getbootstrap.com/){:target="_blank"}
