# Zadaci za vežbu

1.

Napraviti MongoDB bazu podataka sa imenom `shop` koja sadrži kolekciju `Product` sa opisima proizvoda i kolekciju `Order` sa opisima narudžbina. 

Svaki proizvod ima svoj jedinstveni identifikator, obavezno ime i cenu, i opciono, opis proizvoda maksimalne dužine 200 karaktera. 

Svaka narudžbina ima svoj jedinstveni identifikator, niz identifikatora proizvoda koji je čine,  obavezno ime naručioca, njegovu email adresu i adresu isporuke. 

2. 

Osmisliti i implementirati API koji podržava sledeće radnje: 

a) isčitavanje informacija o svim proizvodima

b) isčitavanje informacija o proizvodu sa zadatim identifikatorom

c) kreiranje novog proizvoda

d) ažuriranje informacija proizvoda sa zadatim identifikatorom

e) brisanje proizvoda sa zadatim identifikatorom

f) isčitavanje informacija o svim narudžbinama

g) isčitavanje informacija o narudžbini sa zadatim identifikatorom

h) kreiranje nove narudžbine

i) brisanje narudžbine sa zadatim identifikatorom

U implementaciji se pridržavati podele zaduženja između rutera, modela i kontrolera. 






