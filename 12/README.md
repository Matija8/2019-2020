# 12. sedmica vežbi

## MongoDB sistem za upravljanje bazama podataka i Mongoose.js biblioteka

- [MongoDB](./mongodb.md) 
- [primeri sa časa](./primeri/){:target="_blank"}
- [Video lekcija - Anđelka Zečević](https://youtu.be/MtFCcCG8fL0){:target="_blank"}
- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"} 

## Korisne veze

- [MongoDB uvodni tutorial](https://docs.mongodb.com/manual/tutorial/getting-started/){:target="_blank"}
- [zvanična stranica Mongoose.js biblioteke](https://mongoosejs.com/){:target="_blank"}
