const mongoose = require('mongoose');
const User = require('../models/usersModel');

module.exports.getUsers = async (req, res, next) => {
  // alternativni nacin postavljanja upita i obrade rezultata
  //   User.find({}, (err, users)=>{
  //       if(err){
  //           throw err;
  //       }
  //       res.json(users);
  //   });
  try {
    const users = await User.find({}).exec();
    res.status(200).json(users);
  } catch (err) {
    next(err);
  }
};

module.exports.getUserById = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id).exec();
    if (user) {
      res.status(200);
      res.json(user);
    } else {
      res.status(404);
      res.send();
    }
  } catch (err) {
    next(err);
  }
};

module.exports.createUser = async (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    res.status(400);
    res.send();
  } else {
    try {
      const newUser = new User({
        _id: new mongoose.Types.ObjectId(),
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
      });

      await newUser.save();

      res.status(201).json(newUser);
    } catch (err) {
      next(err);
    }
  }
};

module.exports.updateUserById = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id).exec();

    if (user) {
      const updatePasswordInfo = req.body;

      if (updatePasswordInfo.currentPassword != user.password) {
        res.status(400).send();
      } else {
        // alternativni nacin azuriranja korisnika:
        // user.password = updatePasswordInfo.newPassword;
        // await user.save();

        User.updateOne(
          { _id: req.params.id },
          { $set: { password: updatePasswordInfo.newPassword } }
        );
        res.status(200).send();
      }
    } else {
      res.status(404).send();
    }
  } catch (err) {
    next(err);
  }
};

module.exports.deleteUserById = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id).exec();

    if (user) {
      await User.findByIdAndDelete(req.params.id).exec();
      res.status(200).send();
    } else {
      res.status(404).send();
    }
  } catch (err) {
    next(err);
  }
};
