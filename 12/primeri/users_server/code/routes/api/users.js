const express = require('express');
const router = express.Router();


const controller = require('../../contollers/usersContoroller');

// http://localhost:3000/api/users
router.get('/', controller.getUsers);

// http://localhost:3000/api/users/4
router.get('/:id', controller.getUserById);

// http://localhost:3000/api/users
router.post('/', controller.createUser);

// http://localhost/api:3000/users/4
router.put('/:id', controller.updateUserById);

// http://localhost/api:3000/users/4
router.delete('/:id', controller.deleteUserById);

// error blok - ovaj blok koda je sada pokriven kodom iz app.js fajla 
// router.use((req, res, next)=>{
//     const unknownMethod = req.method;
//     const unknownPath = req.path;
//     res.status(500).json({'message': `Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`});
// });

module.exports = router;
