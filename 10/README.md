# 10. sedmica vežbi

## Node.js

- [Node.js](./nodejs.md) 
- [primeri sa časa](./primeri/){:target="_blank"}
- [Video lekcija - Anđelka Zečević](https://www.youtube.com/watch?v=t1Qls6Bx0nM){:target="_blank"}

<!-- ### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"} -->

## Korisne veze

- [zvanična Node.js dokumentacija](https://nodejs.org/docs/latest-v13.x/api/){:target="_blank"}
- [tutorial Node.js zajednice](https://nodejs.dev/){:target="_blank"}
- [Eloquent JavaScript](https://eloquentjavascript.net/20_node.html){:target="_blank"}
