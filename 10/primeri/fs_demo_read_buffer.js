const fs = require('fs');

fs.readFile('node_intro.txt', (err, buffer) => {
  if (err) {
    throw err;
  }
  console.log('Duzina sadrzaja datoteke: ', buffer.length);

  // datoteka sadrzi tekst "Node.je super!"
  // kod nultog karaktera odgovara ASCII kodu slova N
  console.log('Prvi bajt sadrzaja: ', buffer[0]);
});
