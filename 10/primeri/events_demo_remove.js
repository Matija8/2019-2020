const EventEmitter = require('events');

class SweetEmitter extends EventEmitter{}
const sweetEmitter = new SweetEmitter();

const printMessage = (data)=>{
    console.log('With joy we serve: ', data);
};

sweetEmitter.on('chocolateEvent', printMessage);

sweetEmitter.emit('chocolateEvent', 'cake');
sweetEmitter.emit('chocolateEvent', 'ice cream');

sweetEmitter.removeListener('chocolateEvent', printMessage);

sweetEmitter.emit('chocolateEvent', 'pudding');
