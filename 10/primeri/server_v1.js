const http = require('http');

const server = http.createServer((req, res) => {
    res.writeHead(200, {
      'Content-Type': 'text/html',
    });
  
  const body = '<h1> Hello from Node.js server! </h1>';
  res.write(body);

  res.end();
});


server.listen(5000, () => {
  console.log('Server je pokrenut!');
});
