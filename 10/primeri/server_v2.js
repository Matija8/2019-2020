const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) => {
  const indexPath = path.join(__dirname, 'public', 'index.html');

  fs.readFile(indexPath, (err, content) => {
    if (err) {
      throw err;
    } else {
      res.writeHead(200, {
        'Content-Type': 'text/html',
      });
      res.write(content);
      res.end();
    }
  });
});

server.listen(5000, () => {
  console.log('Server je pokrenut!');
});
