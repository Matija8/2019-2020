# Zadaci za vežbu
1. 

a) Napisati klasu `Post` koja predstavlja jedan post na blogu. Svojstva koja karakterišu post su naslov, ime autora, datum objavljivanja i broj lajkova. 

b) Napraviti niz `blog` koji sadrži nekoliko postova. 

c) Napisati funkciju koja iz kreiranog niza `blog` izdvaja sve postove koji su objavljeni posle zadatog datuma. 

d) Napisati funkciju koja izdvaja sve postove koji imaju više od 50 lajkova.

e) Napisati funkciju koja izdvaja sve postove zadatog autora. 

f) Napisati funkciju koja uređuje postove po poularnosti tj. po broju lajkova.

g) Napisati funkciju koja pronalazi najpopularnijeg autora, tj. autora čiji postovi u zbiru imaju najviše lajkova. 

h) Napisati funkciju koja proverava da li su svi postovi aktuelni, tj. da li su objavljenih u prethodnih tri meseca. 

Prilikom rešavanja koristiti funkcije `map`, `filter`, `every`, `reduce` i `sort`.

2. Napisati funkciju koja koristeći ugrađenu funkciju `reduce` spaja prosleđene nizove. Na primer, za nizov [[3, 4, 5], [6, 7, 8], [9], [10]] generiše se niz [3, 4, 5, 6, 7, 8, 9, 10]. 

3. Napraviti JavaScript niz koji sadrži brojeve od 1 do 10000. Koristići kombinaciju `filter` i `map` ugrađenih funkcija generisati niz svih kvadrata parnih brojeva. Zatim, generisati isti niz korišćenjem `reduce` metode. Kako se ponašaju vremena izvršavanja ovih funkcija? 

4. Konstruisati hladan RxJS tok `array$` koji će na svakih `1.5` sekundi emitovati niz brojeva od `1` do `N`, pri čemu je `N` redni broj niza koji se emituje, tj. emituju se vrednosti: [1], [1, 2], [1, 2, 3], itd. Nakon `10` emitovanih vrednosti, zaustaviti dalje emitovanje vrednosti iz toka.

Napisati i osluškivač koji u konzoli ispisuje sumu elemenata tekućeg emitovanog niza.

5. Konstruisati hladni RxJS tok `random$` koji emituje `10` vrednosti, pri čemu svaka vrednost može biti:
- nasumičan broj iz intervala [1, 10]
- nasumičan karakter iz intervala [‘a’, ‘z’]
- nasumična Bulova vrednost 

Konstruisati hladni tok `product$` koji će emitovati proizvod svih brojeva koji su bili emitovani iz toka `random$`. Na primer, ako su emitovane vrednosti iz toka `random$` bile redom: 
‘a’, false, 3, ‘c’, ‘z’, 5, true, 3, ‘v’, 2, 
onda će tok product$ emitovati redom: 
3, 15, 45, 90.

Napisati i osluškivač koji u konzoli ispisuje emitovane vrednosti iz toka `product$`.

6. Ukoliko dodje do greške u RxJS toku, na koje sve načine se možemo izboriti sa njima? Po čemu su ti načini slični, a po čemu se razlikuju?
