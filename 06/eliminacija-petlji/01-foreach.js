class Student
{
	constructor(indeks, ime, prezime)
	{
		this.indeks = indeks;
		this.ime = ime;
		this.prezime = prezime;
	}
}

let studenti = [
	new Student("1/2017", "Pera", "Peric"),
	new Student("2/2017", "Jovana", "Jovanovic"),
	new Student("3/2017", "Nikola", "Nikolic"),
];

function dohvatiIndekse(studenti)
{
    let indeksi = [];
    
    studenti.forEach(student => {
        indeksi.push(student.indeks);
    });

    return indeksi;
}

console.log(dohvatiIndekse(studenti));
