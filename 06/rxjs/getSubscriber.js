const getSubscriber = (function() {
  function Subscriber(id, transformFn) {
    this.id = id;
    this.transformFn = transformFn;
    this.next = function(emittedValue) {
      if (typeof this.transformFn === 'function') {
        console.log(`${this.id}: ${this.transformFn(emittedValue)}`);
      } else {
        console.log(`${this.id}: ${emittedValue}`);
      }
    };
    this.error = function(error) {
      console.log(`${this.id}: An Error occured - ${error.message || error}`);
    };
    this.complete = function() {
      console.log(`${this.id}: Completed!`);
    };
  }

  return (id, transformFn) => {
    return new Subscriber(id, transformFn);
  };
})();
