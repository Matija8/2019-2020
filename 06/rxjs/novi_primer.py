#!/usr/bin/python

import sys

def main():
    tekst = '''<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RxJS ''' + ' '.join([rec.capitalize() for rec in sys.argv[1].split('.')]) + '''</title>
</head>

<body>
    <script src="rxjs.umd.js"></script>
    <script src="getSubscriber.js"></script>
    <script>
        
    </script>
</body>

</html>
'''

    with open(sys.argv[1] + '.html', 'w') as f:
        f.write(tekst)


if __name__=='__main__':
    main()
    print('done')