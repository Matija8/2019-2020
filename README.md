# Programiranje za veb 2019/2020 - vežbe

Nastavno osoblje:

- Profesor: [Ivan Čukić](http://poincare.matf.bg.ac.rs/~ivan/)
- Asistenti: [Nikola Ajzenhamer](http://poincare.matf.bg.ac.rs/~nikola_ajzenhamer/), [Anđelka Zečević](http://www.math.rs/~andjelkaz), [Jelena Marković](http://www.matf.bg.ac.rs/p/jelena-markovic/pocetna/)

## Sadržaj vežbi

- [1. sedmica](./01/README.md)
- [2. sedmica](./02/README.md)
- [3. sedmica](./03/README.md)
- [4. sedmica](./04/README.md)
- [5. sedmica](./05/README.md)
- [6. sedmica](./06/README.md)
- [7. sedmica](./07/README.md)
- [8. sedmica](./08/README.md)
- [9. sedmica](./09/README.md)
- [10. sedmica](./10/README.md)
- [11. sedmica](./11/README.md)
- [12. sedmica](./12/README.md)
- [13. sedmica](./13/README.md)

## Primer ispita

- [Informacije o primeru ispita](./primer%20ispita/README.md)

## Zadaci za ispita

- [Rokovi](./ispiti/README.md)
