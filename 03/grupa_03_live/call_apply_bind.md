
U prethodnim primerima smo videli da vrednost objekta `this` u funkcijama zavisi od konteksta i načina na koji se funkcija poziva. Sledeći primeri ilustruju ideje u kojima se funkcije mogu pozajmljivati i pozivati sa eksplicitno navedenim `this` objektima. 

# Funkcije call i apply

Funkcije `call` i `apply` omogućavaju poziv funkcije uz navođenje objekta koji igra ulogu `this` objekta. Pored ovog objekta, mogu se navesti i sami argumenti funkcije i to, pojedinačno, jedan za drugim, u slučaju funkcije `call`, ili u formi niza, u slučaju funkcije `apply`. 

## Primer 1.

Posmatrjmo funkciju `print` koja ispisuje poruku na osnovu vrednosti globalne promenljive `message` i argumenta `name`.
<pre>
var message = 'Hello';
function print(name) {
  // this referise na globalni objekat
  console.log(`${this.message} ${name}!`);
}
</pre>

Uobičajeni poziv funkcije `print` koji ispisuje poruku `'Hello Ana!'` je
<pre>
print('Ana');
</pre>

Ekvivalentni poziv prethodnom pozivu uz korišćenje funkcije `call` je 
<pre>
print.call({ message: 'Hello' }, 'Ana');
</pre>
gde se preko objekta `{message: 'Hello'}` zadaje vrednost `this` objekta, a preko vrednosti `'Ana'` argument funkcije.

Ovakva priroda funkcije `call` dozvoljava poziv funkcije `print` sa prilagođenom vrednošću `this` objekta npr. onom koja sadrži izmenjenu poruku: 
<pre>
print.call({ message: 'Zdravo' }, 'Ana');
</pre>

Funkcija `apply` omogućava istu ovu funkcionalnost, ali očekuje argumente funkcije koja se poziva, u našem slučaju `print`, u formi niza: 
<pre>
print.apply({ message: 'Zdravo' }, ['Ana']);
</pre>


## Primer 2. 

Objekat `arithmetics` sadrži svojstvo `n` i dve metode `sum` i `mul` koje, redom, izvršavaju sabiranje i množenje vrednosti `n` sa zadatim argumentom `m`. 

<pre>
let arithmetics = {
  n: 10,
  sum: function(m) {
    return this.n + m;
  },
  mul: function(m) {
    return this.n * m;
  }
};
</pre>

Uobičajeni poziv metode `sum` sa argumentom `10` je
<pre>
arithmetics.sum(10);
</pre>
i rezultira vrednošću 20. Slično, uobičajeni poziv metode `mul` sa argumentom `3` je
<pre>
arithmetics.mul(3);
</pre>
i rezultira vrednošću 30.


Poziv metode `sum` u kojem objekat `{n:30}` igra ulugu `this` objekta, a vrednost `10` argument `m` funkcije `sum` zapisan pomoću funkcije `call` je 
<pre>
arithmetics.sum.call({ n: 30 }, 10);
</pre>
Ovaj poziv rezultira vrednošću 40. 

Poziv metode `mul` u kojem objekat `{n:15}` igra ulugu `this` objekta, a vrednost `4` argument `m` funkcije `mul` zapisan pomoću funkcije `call` je 
<pre>
arithmetics.mul.call({ n: 15 }, 4);
</pre>
Ovaj poziv rezultira vrednošću 60. 

# Funkcija bind
Funkcija `bind` omogućava kreiranje nove funkcije sa razrešenom vrednošću `this` objekta. 

Na primer, za funkciju `print` navedenu u prvom primeru, fragment koda 
<pre>
let printResolved = print.bind({ message: 'Zdravo' });
</pre>
kreira novu funkciju `printResolved` u kojoj je `this` objekat `{message: 'Zdravo'}`.  Poziv funkcije 
<pre>
printResolved('Ana');
</pre> 
će očekivano ispisati poruku `Zdravo Ana`. 

Slično, za metod `sum` iz primera 2, fragment koda
<pre>
let sum30 = arithmetics.sum.bind({ n: 30 });
</pre>
kreira funkciju `sum30` u kojoj je vrednost `this` objekta `{n: 30}`. Stoga će poziv
<pre>
sum30(6);
</pre>
generisati rezultat `36`.

`bind` funkcija dozvoljava i razrešavanje vrednosti argumenata funkcija. Na primer, fragment koda 
<pre>
let sum35 = arithmetics.sum.bind({ n: 30 }, 5);
</pre>
kreira funkciju `sum35` u kojoj je vrednost `this` objekta `{n: 30}`, a vrednost argumenta `m` fiksirana na `5`.  Stoga će pozivi ove funkcije oblika `sum35()` ili sa navedenim argumentom, na primer, `sum35(6)` uvek rezultirati vrednošću `35`.


Zbog prirode `this` objekta i praksi koje jezik `JavaScript` nalaže, potreba za korišćenjem ovih funkcija je česta.

# Literatura
Više o ovim funkcijama se može pročitati u zvaničnoj MDN dokumentaciji na adresama [call](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call), [apply](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/apply) i [bind](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind).