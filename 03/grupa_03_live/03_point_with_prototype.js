// Naredni kod izvrsavati u pregledacu.

function Point(x, y) {
  this.x = x;
  this.y = y;
  this.print = function() {
    console.log('Point: ', this.x, this.y);
  };
}

A = new Point(4, 5);

console.log(A.__proto__);
console.log(Object.getPrototypeOf(A));
console.log(A.__proto__ == Object.getPrototypeOf(A));
typeof A.__proto__.constructor;

// Svojstva i metodi prototipa su vidljivi na nivou svake instance 
// kreirane uparenom konstruktorskom funkcijom.
Point.prototype.color = 'red';

B = new Point(10, 2);
console.log(A.color);
console.log(B.color);
A.__proto__.color = 'blue';
console.log(B.color);

// Provera da li je svojstvo lokalno, na nivou instance, ili prisutno u lancu prototipova
// je moguca kroz hasOwnProperty() metodu.
console.log(A.hasOwnProperty('color'));
console.log(A.hasOwnProperty('x'));

// Dodavanje svojstava se uvek realizuje lokalno, na nivou instance.
A.z = 10;
console.log(A.z);
console.log(B.z);

// Brisanje svojstava se realizuje uvek lokalno, na nivou instance.
delete A.z;

// Tema za razmisljanje:
// Koja konstruktorska funkcija je kreirala Point.prototype?
typeof Point.prototype;

console.log(Point.prototype.__proto__);

// Na kraju lanca ove hijerarhije se nalazi null.
console.log(Point.prototype.__proto__.__proto__);
