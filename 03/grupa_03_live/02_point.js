function Point(x, y) {
  // Ukoliko se funkcija pozove kao konstruktorska funkcija, inicijalni korak je
  // this = {}

  // U suprotnom, this referise na neki drugi objekat.

  this.x = x;
  this.y = y;
  this.print = function() {
    console.log('Point: ', this.x, this.y);
  };

  // Ukoliko se funkcija pozove kao konstruktorska funkcija, poslednji korak je
  // return this;

  // U suprotnom, funkcija vraca vrednost undefined.
}

let A = new Point(4, 5);
console.log(A.x);
console.log(A.y);
A.print();
console.log(A instanceof Point);

let B = Point(1, 1);
console.log(B);
// console.log(B.x);
// console.log(B.y);
// B.print();
console.log(B instanceof Point);

// Popravljena konstruktorska funkcija tako da uvek kreira novi objekat,
// bez obzira da li se poziva sa ili bez new operatora.

function PointSafe(x, y) {
  if (!(this instanceof Point)) {
    return new Point(x, y);
  }

  this.x = x;
  this.y = y;
  this.print = function() {
    console.log('Point: ', this.x, this.y);
  };
}

let C = new PointSafe(6, 7);
C.print();

let D = PointSafe(10, 10);
D.print();
