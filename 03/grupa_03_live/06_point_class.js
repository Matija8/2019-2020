class Point{
  constructor(x, y, color){
    this.x = x;
    this.y = y;
    this.color = color;
  }
 
  print(){
    console.log(this.x, this.y, this.color);
  }
  
  translate(v){
    this.x += v;
    this.y += v;    
  }
}

let a = new Point(10, 20, 'red');
a.translate(4);
a.print();

// Proveriti rezultat prevodjenja ovog koda na 
// https://babeljs.io/