// Primer 1. 
function Point(x, y) {
  this.x = x;
  this.y = y;
}
Point.prototype.print = function() {
  console.log('Point: ', this.x, this.y);
};

let A = new Point(5, 6); 
A.print();
Point.prototype = { color : 'red'};
console.log(A.color);

let B = new Point(2, 3);
console.log(B.color);
// B.print();

console.log('A: ', A instanceof Point);
console.log(A.__proto__);
console.log('B: ', B instanceof Point);
console.log(B.__proto__);

Point.prototype.constructor = Point;



// Primer 2.
// Ugradjeni JS prototipi
let a = [2, 3, 4, 5, 6];
console.log(a.reverse());

Array.prototype.reverse = function() {
  console.log('Heh, necu da radim!');
};

a.reverse();
